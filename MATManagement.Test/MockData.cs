﻿using MATManagement.DataModels;
using System;
using System.Collections.Generic;

namespace MATManagement.Test
{
    public class MockData
    {
        private SchoolDataset DefaultSchoolDataset = new SchoolDataset
        {
            SchoolShortName = "Woodfields",
            SchoolFullName = "Woodfields Infant and Junior School",
            SchoolUrn = 303406,
            DatasetTableName = "Test",
            DatasetName = "Exclusionseer",
            ExtractDate = DateTime.Now,
            PeriodStart = DateTime.Now,
            PeriodEnd = DateTime.Now,
            UpdatedAt = DateTime.Now,
        };


        public WrapperModel InvalidDataTableName => new WrapperModel()
        {
            SchoolDataset = DefaultSchoolDataset,

            Data = new List<object>
               {
                   new Pupil
                   {
                     SchoolUrn= 1,
                     PupilId= 2,
                     YearGroup= "Yr12",
                     Gender= "M",
                     SENProvision= "None",
                     EAL= "N",
                     EthnicGroup= "Whit",
                     EthnicityMainCode= "WBRI",
                     FSM= "Y",
                     PupilPremium= "N",
                     FirstName= "Y",
                     LastName= "Y",
                     Dob= DateTime.Now,
                     TermOfBirth= "TermOfBirth",
                     EthnicityCode= "EthnicityCode",
                     Ethnicity= "Ethnicity",
                     FirstLanguage= "FirstLanguage",
                     EnglishProficiency= "EnglishProficiency"
                   }
               }
        };

        public WrapperModel InvalidValuesInDatasets
        {
            get
            {
                DefaultSchoolDataset.DatasetTableName = "Pupil";

                return new WrapperModel()
                {
                    SchoolDataset = DefaultSchoolDataset,

                    Data = new List<object>{
                   new Pupil
                   {
                     SchoolUrn= 1,
                     PupilId= 2,
                     YearGroup= "Yr1234",
                     Gender= "4",
                     SENProvision= "None",
                     EAL= "N",
                     EthnicGroup= "Whit",
                     EthnicityMainCode= "WBRI",
                     FSM= "Y",
                     PupilPremium= "N",
                     FirstName= "first name",
                     LastName= "last name",
                     Dob= DateTime.Now,
                     TermOfBirth= "TermOfBirth",
                     EthnicityCode= "EthnicityCode",
                     Ethnicity= "Ethnicity",
                     FirstLanguage= "FirstLanguage",
                     EnglishProficiency= "EnglishProficiency"
                   }
               }
                };
            }
        }

        public WrapperModel InvalidObjectByCrossDataTableNameAndDatasets
        {
            get
            {
                DefaultSchoolDataset.DatasetTableName = "PupilExclusions";

                return new WrapperModel()
                {
                    SchoolDataset = DefaultSchoolDataset,

                    Data = new List<object>{
                   new
                   {
                     SchoolUrn= 1,
                     PupilId= 2,
                     YearGroup= "Yr1234",
                     Gender= "4",
                     SENProvision= "None",
                     EAL= "N",
                     EthnicGroup= "Whit",
                     EthnicityMainCode= "WBRI",
                     FSM= "Y",
                     PupilPremium= "N",
                     FirstName= "first name",
                     LastName= "last name",
                     Dob= DateTime.Now,
                     TermOfBirth= "TermOfBirth",
                     EthnicityCode= "EthnicityCode",
                     Ethnicity= "Ethnicity",
                     FirstLanguage= "FirstLanguage",
                     EnglishProficiency= "EnglishProficiency"
                   }
               }
                };
            }
        }


        public WrapperModel ValidPupilObjectAndDatasets
        {
            get
            {
                DefaultSchoolDataset.DatasetTableName = "Pupil";

                return new WrapperModel()
                {
                    SchoolDataset = DefaultSchoolDataset,

                    Data = new List<object>{
                   new Pupil
                   {
                     SchoolUrn= 1,
                     PupilId= 2,
                     YearGroup= "Yr12",
                     Gender= "M",
                     SENProvision= "None",
                     EAL= "N",
                     EthnicGroup= "Whit",
                     EthnicityMainCode= "WBRI",
                     FSM= "Y",
                     PupilPremium= "N",
                     FirstName= "first name",
                     LastName= "last name",
                     Dob= DateTime.Now,
                     TermOfBirth= "TermOfBirth",
                     EthnicityCode= "12",
                     Ethnicity= "Ethnicity",
                     FirstLanguage= "FirstLanguage",
                     EnglishProficiency= "EnglishProficiency"
                   }
               }
                };
            }
        }
    }
}
