using MATManagement.Controllers;
using MATManagement.DatabaseService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace MATManagement.Test
{
    public class HomeControllerTest
    {
        private readonly MatDbContext dbContext;
        private readonly Repository repos;
        private readonly Mock<ILogger<HomeController>> logger;
        private readonly HomeController homeController;


        public HomeControllerTest()
        {
            dbContext = new MatDbContext(new DbContextOptionsBuilder<MatDbContext>().UseInMemoryDatabase(databaseName: "MATManagement").Options);
            repos = new Repository(dbContext);
            logger = new Mock<ILogger<HomeController>>();
            homeController = new HomeController(repos, logger.Object);
        }

        /// <summary>
        /// OkResult Set for GetAll Action.
        /// </summary>
        [Fact]
        public async void GetAll_SchoolData_ReturnOkResult()
        {
            var schoolDatasetsResult = await homeController.GetAll();

            Assert.IsType<OkObjectResult>(schoolDatasetsResult);
        }

        /// <summary>
        /// Valid Inset with 201 Status, Active & Dataset count equal to 1
        /// </summary>
        [Fact]
        public async void Add_Valid_SchoolDataset_Pupil_OkResult()
        {
            // Arrange
            var data = new MockData().ValidPupilObjectAndDatasets;

            var publiDatasetCount = data.Data.Count;

            var result = await homeController.Add(data);

            var storedSchoolDataset = await dbContext.SchoolDatasets.FirstOrDefaultAsync(x => x.SchoolUrn == data.SchoolDataset.SchoolUrn && x.DatasetTableName == data.SchoolDataset.DatasetTableName); var storedPupilCount = await dbContext.Pupils.CountAsync();

            // Assert
            Assert.Equal(201, ((ObjectResult)result).StatusCode);

            Assert.Equal(true, storedSchoolDataset?.Ative);

            Assert.Equal(publiDatasetCount, storedPupilCount);


        }

        /// <summary>
        /// Test Valid DatasetTableName 
        /// </summary>
        [Fact]
        public async void Add_InvalidDataTableName_ReturnsBadRequest()
        {
            // Arrange
            var data = new MockData().InvalidDataTableName;

            var result = await homeController.Add(data);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }


        /// <summary>
        /// Test to Validate invalid data.
        /// </summary>
        [Fact]
        public async void Add_InvalidDataset_ReturnsBadRequest()
        {
            // Arrange
            var data = new MockData().InvalidValuesInDatasets;

            var result = await homeController.Add(data);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        /// <summary>
        /// Test to check cross DatasetTableName and data
        /// e.g. DatatableName is Pupil & data had list of PupilAttentances.
        /// </summary>
        [Fact]
        public async void Add_InvalidDataset_Cross_SetDataTableName_and_Dataset_Returns422StatusCode()
        {
            // Arrange
            var data = new MockData().InvalidObjectByCrossDataTableNameAndDatasets;

            var result = await homeController.Add(data);

            ObjectResult statusCodeResult = result as ObjectResult;

            // Assert
            Assert.IsType<ObjectResult>(result);

            Assert.Equal(422, statusCodeResult.StatusCode);
        }

        
    }
}
