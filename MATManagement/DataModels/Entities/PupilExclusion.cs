﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MATManagement.DataModels
{
    [Table("PupilExclusions")]
    public class PupilExclusion : PupilBase
    {
        [Required(ErrorMessage = "StartDate is required.")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "EndDate is required.")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "ExclusionType is required.")]
        [MaxLength(15, ErrorMessage = "ExclusionType allowed max 15 character only.")]
        public string ExclusionType { get; set; }

        [Required(ErrorMessage = "ExclusionReason is required.")]
        [MaxLength(150, ErrorMessage = "ExclusionReason allowed max 150 character only.")]
        public string ExclusionReason { get; set; }

        [Required(ErrorMessage = "ExclusionSessions is required.")]
        public int ExclusionSessions { get; set; }

        [Required(ErrorMessage = "ExclusionDays is required.")]
        public int ExclusionDays { get; set; }



        public SchoolDataset SchoolDataset { get; set; }
    }
}
