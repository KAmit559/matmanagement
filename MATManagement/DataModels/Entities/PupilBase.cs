﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MATManagement.DataModels
{
    public class PupilBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        public int SchoolDatasetId { get; set; }

        [Required(ErrorMessage = "SchoolUrn is required.")]
        public int SchoolUrn { get; set; }

        [Required(ErrorMessage = "PupilId is required.")]
        public int PupilId { get; set; }

        [Required(ErrorMessage = "YearGroup is required.")]
        [MaxLength(5, ErrorMessage = "YearGroup allowed max 5 character only.")]
        public string YearGroup { get; set; }

        [Required(ErrorMessage = "Gender is required.")]
        [MaxLength(1, ErrorMessage = "Gender allowed max 1 character only.")]
        [StringRange(AllowableValues = new[] { "M", "F", "X", "U" }, ErrorMessage = "Value must be either M/F/X/U")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "SENProvision is required.")]
        [MaxLength(15, ErrorMessage = "SENProvision allowed max 15 character only.")]
        public string SENProvision { get; set; }

        [Required(ErrorMessage = "EAL is required.")]
        [MaxLength(1, ErrorMessage = "EAL allowed max 1 character only.")]
        [StringRange(AllowableValues = new[] { "Y", "N", "U" }, ErrorMessage = "Value must be either Y/N/U")]
        public string EAL { get; set; }

        [Required(ErrorMessage = "EthnicGroup is required.")]
        [MaxLength(4, ErrorMessage = "EthnicGroup allowed max 4 character only.")]
        public string EthnicGroup { get; set; }

        [Required(ErrorMessage = "EthnicityMainCode is required.")]
        [MaxLength(4, ErrorMessage = "EthnicityMainCode allowed max 4 character only.")]
        public string EthnicityMainCode { get; set; }

        [Required(ErrorMessage = "FSM is required.")]
        [MaxLength(1, ErrorMessage = "FSM allowed max 1 character only.")]
        [StringRange(AllowableValues = new[] { "Y", "N"}, ErrorMessage = "Value must be either Y/N")]
        public string FSM { get; set; }

        [Required(ErrorMessage = "PupilPremium is required.")]
        [MaxLength(1, ErrorMessage = "PupilPremium allowed max 1 character only.")]
        [StringRange(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "Value must be either Y/N")]
        public string PupilPremium { get; set; }
    }
}
