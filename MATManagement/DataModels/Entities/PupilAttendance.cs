﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MATManagement.DataModels
{
    [Table("PupilAttendances")]
    public class PupilAttendance : PupilBase
    {

        [Required(ErrorMessage = "PeriodStart is required.")]
        public DateTime PeriodStart { get; set; }

        [Required(ErrorMessage = "PeriodEnd is required.")]
        public DateTime PeriodEnd { get; set; }

        [Required(ErrorMessage = "PeriodName is required.")]
        [MaxLength(25, ErrorMessage = "PeriodName allowed max 25 character only.")]
        public string PeriodName { get; set; }

        [Required(ErrorMessage = "Sessions is required.")]
        [MaxLength(100, ErrorMessage = "Sessions allowed max 100 character only.")]
        public string Sessions { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsPresent is required.")]
        public int SessionsPresent { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsAea is required.")]
        public int SessionsAea { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsAbsentAuth is required.")]
        public int SessionsAbsentAuth { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsAbsentUnauth is required.")]
        public int SessionsAbsentUnauth { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsLatePresent is required.")]
        public int SessionsLatePresent { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Invalid value for integer type")]
        [Required(ErrorMessage = "SessionsLateAbsent is required.")]
        public int SessionsLateAbsent { get; set; }




        public virtual SchoolDataset SchoolDataset { get; set; }
    }



}
