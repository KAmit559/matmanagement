﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MATManagement.DataModels
{
    [Table("SchoolDatasets")]
    public class SchoolDataset
    {
        public SchoolDataset()
        {
            this.PupilExclusions = new List<PupilExclusion>();
            this.PupilAttendances = new List<PupilAttendance>();
            this.Pupils = new List<Pupil>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [BindNever]
        public bool Ative { get; set; }

        [Required(ErrorMessage = "SchoolShortName is required.")]
        [MaxLength(25, ErrorMessage = "SchoolShortName allowed max 25 character only.")]
        public string SchoolShortName { get; set; }


        [Required(ErrorMessage = "SchoolFullName is required.")]
        [MaxLength(100, ErrorMessage = "SchoolFullName allowed max 100 character only.")]
        public string SchoolFullName { get; set; }

        [Required(ErrorMessage = "SchoolUrn is required.")]
        public int SchoolUrn { get; set; }

        [Required(ErrorMessage = "DatasetTableName is required.")]
        [MaxLength(30, ErrorMessage = "DatasetTableName allowed max 30 character only.")]
        [StringRange(AllowableValues = new[] { "PupilExclusions", "PupilAttendance", "Pupil" }, ErrorMessage = "Invalid datasetTableName has been provided.")]
        public string DatasetTableName { get; set; }

        [Required(ErrorMessage = "DatasetName is required.")]
        [MaxLength(500, ErrorMessage = "DatasetName allowed max 500 character only.")]
        public string DatasetName { get; set; }

        [Required(ErrorMessage = "ExtractDate is required.")]
        public DateTime ExtractDate { get; set; }

        [Required(ErrorMessage = "PeriodStart is required.")]
        public DateTime PeriodStart { get; set; }

        [Required(ErrorMessage = "PeriodEnd is required.")]
        public DateTime PeriodEnd { get; set; }

        [MaxLength(500, ErrorMessage = "Description allowed max 500 character only.")]
        public string Description { get; set; }

        [BindNever]
        public DateTime UpdatedAt { get; set; }

        [BindNever]
        [MaxLength(250)]
        public string UpdatedBy { get; set; }



        [BindNever]
        [JsonIgnore]
        public virtual List<PupilExclusion> PupilExclusions { get; set; }

        [BindNever]
        [JsonIgnore]
        public List<PupilAttendance> PupilAttendances { get; set; }

        [BindNever]
        [JsonIgnore]
        public List<Pupil> Pupils { get; set; }

    }
}
