﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MATManagement.DataModels
{
    [Table("Pupils")]
    public class Pupil : PupilBase
    {
        [Required(ErrorMessage = "FirstName is required.")]
        [MaxLength(250, ErrorMessage = "FirstName allowed max 250 character only.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required.")]
        [MaxLength(250, ErrorMessage = "LastName allowed max 250 character only.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Dob is required.")]
        public DateTime Dob { get; set; }

        [Required(ErrorMessage = "TermOfBirth is required.")]
        [MaxLength(15, ErrorMessage = "TermOfBirth allowed max 15 character only.")]
        public string TermOfBirth { get; set; }

        [Required(ErrorMessage = "EthnicityCode is required.")]
        [MaxLength(4, ErrorMessage = "EthnicityCode allowed max 4 character only.")]
        public string EthnicityCode { get; set; }

        [Required(ErrorMessage = "Ethnicity is required.")]
        [MaxLength(50, ErrorMessage = "Ethnicity allowed max 50 character only.")]
        public string Ethnicity { get; set; }

        [Required(ErrorMessage = "FirstLanguage is required.")]
        [MaxLength(50, ErrorMessage = "FirstLanguage allowed max 50 character only.")]
        public string FirstLanguage { get; set; }

        [Required(ErrorMessage = "EnglishProficiency is required.")]
        [MaxLength(25, ErrorMessage = "EnglishProficiency allowed max 25 character only.")]
        public string EnglishProficiency { get; set; }



        public SchoolDataset SchoolDataset { get; set; }
    }
}
