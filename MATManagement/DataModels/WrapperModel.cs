﻿using MATManagement.DataModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MATManagement.DataModels
{
    public class WrapperModel
    {
        [JsonProperty("dataset")]
        public SchoolDataset SchoolDataset { get; set; }

        /// <summary>
        /// Contain a list of one of the models i.e. PupilAttendance or PupilExclusion or Pupil
        /// <remarks>
        ///  ### PupilExclusions ###
        ///       [
        ///         {
        ///             "SchoolUrn": 303406,
        ///             "PupilId": 307,
        ///             "YearGroup": "Yr12e",
        ///             "Gender": "M",
        ///             "SENProvision": "None",
        ///             "EAL": "N",
        ///             "EthnicGroup": "Whit",
        ///             "EthnicityMainCode": "WBRI",
        ///             "FSM": "Y",
        ///             "PupilPremium": "Y",
        ///             "StartDate": "2017-12-07",
        ///             "EndDate": "2017-12-09",
        ///             "ExclusionType": "Fixed Term",
        ///             "ExclusionReason": "Drug and alcohol related",
        ///             "ExclusionSessions": 6,
        ///             "ExclusionDays": 3
        ///         }
        ///       ]
        /// 
        /// OR
        /// 
        /// ### PupilAttendance ###
        ///       [
        ///         {
        ///             "SchoolUrn": 1,
        ///             "PupilId": 2,
        ///             "YearGroup": "Yr12",
        ///             "Gender": "M",
        ///             "SENProvision": "None",
        ///             "EAL": "N",
        ///             "EthnicGroup": "Whit",
        ///             "EthnicityMainCode": "WBRI",
        ///             "FSM": "Y",
        ///             "PupilPremium": "Y",
        ///             "PeriodStart": "12/12/12",
        ///             "PeriodEnd": "12/12/12",
        ///             "PeriodName": "PeriodName",
        ///             "Sessions": "Sessions",
        ///             "SessionsPresent": "2",
        ///             "SessionsAea": "2",
        ///             "SessionsAbsentAuth": "3",
        ///             "SessionsAbsentUnauth": "4",
        ///             "SessionsLatePresent": "5",
        ///             "SessionsLateAbsent": "Y"
        ///         }
        ///       ]        
        /// 
        /// OR
        /// 
        /// ### Pupil ###
        ///       [
        ///         {
        ///             "SchoolUrn": 2,
        ///             "PupilId": 32,
        ///             "YearGroup": "Yr12",
        ///             "Gender": "M",
        ///             "SENProvision": "None",
        ///             "EAL": "N",
        ///             "EthnicGroup": "Whit",
        ///             "EthnicityMainCode": "WBRI",
        ///             "FSM": "Y",
        ///             "PupilPremium": "Y",
        ///             "FirstName": "Y",
        ///             "LastName": "Y",
        ///             "Dob": "Y",
        ///             "TermOfBirth": "Y",
        ///             "EthnicityCode": "Y",
        ///             "Ethnicity": "Y",
        ///             "FirstLanguage": "Y",
        ///             "EnglishProficiency": "Y",
        ///         }
        ///       ]
        /// </remarks>
        /// </summary>
        [JsonProperty("data")]
        public List<object> Data { get; set; }
    }

}
