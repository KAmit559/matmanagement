﻿using System.Collections.Generic;

namespace MATManagement.DataModels
{
    public class ErrorModel
    {
        public string Message { get; set; }

        public List<string> Errors { get; set; }
    }
}

