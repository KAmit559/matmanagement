﻿using MATManagement.DataModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MATManagement.DatabaseService
{
    public class Repository
    {
        #region Readonly Member

        private readonly MatDbContext db;

        #endregion

        #region Constructor

        public Repository(MatDbContext dbContext) => db = dbContext;

        #endregion

        #region GET

        /// <summary>
        /// Return all SchoolDatasets includes ACTIVE and INACTIVE both
        /// </summary>
        /// <returns></returns>
        public Task<List<SchoolDataset>> GetAllSchoolDatasets() => db.SchoolDatasets.ToListAsync();

        /// <summary>
        /// Return only ACTIVE SchoolDatasets
        /// </summary>
        /// <returns></returns>
        public Task<List<SchoolDataset>> GetAllActiveSchoolDatasets() => db.SchoolDatasets.Where(s => s.Ative == true).ToListAsync();
          
        #endregion

        #region INSERT

        /// <summary>
        /// Insert SchoolDataset and information in related tables per DatasetTableName.
        /// Also make existing SchoolDataset INACTIVE based SchoolUrn + DatasetTableName key combination.
        /// </summary>
        /// <param name="schoolDataset"></param>
        /// <returns></returns>
        public async Task<int> AddDataset(SchoolDataset schoolDataset)
        {
            // For each SchoolUrn + DatasetTableName combination there should be at most one record with Active = 1.
            var existingDataset = db.SchoolDatasets.Where(x => x.DatasetTableName.ToUpper() == schoolDataset.DatasetTableName.ToUpper() && x.SchoolUrn == schoolDataset.SchoolUrn).ToList();

            existingDataset?.ForEach(x => x.Ative = false);

            // Insert
            await db.SchoolDatasets.AddAsync(schoolDataset);

            // Commit changes to database
            return await db.SaveChangesAsync();

        }
        #endregion
    }
}
