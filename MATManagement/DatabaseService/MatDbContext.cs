﻿using MATManagement.DataModels;
using Microsoft.EntityFrameworkCore;

namespace MATManagement
{
    public class MatDbContext : DbContext
    {
        public MatDbContext(DbContextOptions<MatDbContext> options) : base(options) { }

        public DbSet<SchoolDataset> SchoolDatasets { get; set; }
        public DbSet<PupilExclusion> PupilExclusions { get; set; }

        public DbSet<PupilAttendance> PupilAttendances { get; set; }

        public DbSet<Pupil> Pupils { get; set; }
    }
}
