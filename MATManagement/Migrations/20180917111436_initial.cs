﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MATManagement.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SchoolDatasets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ative = table.Column<bool>(nullable: false),
                    SchoolShortName = table.Column<string>(maxLength: 25, nullable: false),
                    SchoolFullName = table.Column<string>(maxLength: 100, nullable: false),
                    SchoolUrn = table.Column<int>(nullable: false),
                    DatasetTableName = table.Column<string>(maxLength: 30, nullable: false),
                    DatasetName = table.Column<string>(maxLength: 500, nullable: false),
                    ExtractDate = table.Column<DateTime>(nullable: false),
                    PeriodStart = table.Column<DateTime>(nullable: false),
                    PeriodEnd = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolDatasets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PupilAttendances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SchoolDatasetId = table.Column<int>(nullable: false),
                    SchoolUrn = table.Column<int>(nullable: false),
                    PupilId = table.Column<int>(nullable: false),
                    YearGroup = table.Column<string>(maxLength: 5, nullable: false),
                    Gender = table.Column<string>(maxLength: 1, nullable: false),
                    SENProvision = table.Column<string>(maxLength: 15, nullable: false),
                    EAL = table.Column<string>(maxLength: 1, nullable: false),
                    EthnicGroup = table.Column<string>(maxLength: 4, nullable: false),
                    EthnicityMainCode = table.Column<string>(maxLength: 4, nullable: false),
                    FSM = table.Column<string>(maxLength: 1, nullable: false),
                    PupilPremium = table.Column<string>(maxLength: 1, nullable: false),
                    PeriodStart = table.Column<DateTime>(nullable: false),
                    PeriodEnd = table.Column<DateTime>(nullable: false),
                    PeriodName = table.Column<string>(maxLength: 25, nullable: false),
                    Sessions = table.Column<string>(maxLength: 100, nullable: false),
                    SessionsPresent = table.Column<int>(nullable: false),
                    SessionsAea = table.Column<int>(nullable: false),
                    SessionsAbsentAuth = table.Column<int>(nullable: false),
                    SessionsAbsentUnauth = table.Column<int>(nullable: false),
                    SessionsLatePresent = table.Column<int>(nullable: false),
                    SessionsLateAbsent = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PupilAttendances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PupilAttendances_SchoolDatasets_SchoolDatasetId",
                        column: x => x.SchoolDatasetId,
                        principalTable: "SchoolDatasets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PupilExclusions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SchoolDatasetId = table.Column<int>(nullable: false),
                    SchoolUrn = table.Column<int>(nullable: false),
                    PupilId = table.Column<int>(nullable: false),
                    YearGroup = table.Column<string>(maxLength: 5, nullable: false),
                    Gender = table.Column<string>(maxLength: 1, nullable: false),
                    SENProvision = table.Column<string>(maxLength: 15, nullable: false),
                    EAL = table.Column<string>(maxLength: 1, nullable: false),
                    EthnicGroup = table.Column<string>(maxLength: 4, nullable: false),
                    EthnicityMainCode = table.Column<string>(maxLength: 4, nullable: false),
                    FSM = table.Column<string>(maxLength: 1, nullable: false),
                    PupilPremium = table.Column<string>(maxLength: 1, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    ExclusionType = table.Column<string>(maxLength: 15, nullable: false),
                    ExclusionReason = table.Column<string>(maxLength: 150, nullable: false),
                    ExclusionSessions = table.Column<int>(nullable: false),
                    ExclusionDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PupilExclusions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PupilExclusions_SchoolDatasets_SchoolDatasetId",
                        column: x => x.SchoolDatasetId,
                        principalTable: "SchoolDatasets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pupils",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SchoolDatasetId = table.Column<int>(nullable: false),
                    SchoolUrn = table.Column<int>(nullable: false),
                    PupilId = table.Column<int>(nullable: false),
                    YearGroup = table.Column<string>(maxLength: 5, nullable: false),
                    Gender = table.Column<string>(maxLength: 1, nullable: false),
                    SENProvision = table.Column<string>(maxLength: 15, nullable: false),
                    EAL = table.Column<string>(maxLength: 1, nullable: false),
                    EthnicGroup = table.Column<string>(maxLength: 4, nullable: false),
                    EthnicityMainCode = table.Column<string>(maxLength: 4, nullable: false),
                    FSM = table.Column<string>(maxLength: 1, nullable: false),
                    PupilPremium = table.Column<string>(maxLength: 1, nullable: false),
                    FirstName = table.Column<string>(maxLength: 250, nullable: false),
                    LastName = table.Column<string>(maxLength: 250, nullable: false),
                    Dob = table.Column<DateTime>(nullable: false),
                    TermOfBirth = table.Column<string>(maxLength: 15, nullable: false),
                    EthnicityCode = table.Column<string>(maxLength: 4, nullable: false),
                    Ethnicity = table.Column<string>(maxLength: 50, nullable: false),
                    FirstLanguage = table.Column<string>(maxLength: 50, nullable: false),
                    EnglishProficiency = table.Column<string>(maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pupils", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pupils_SchoolDatasets_SchoolDatasetId",
                        column: x => x.SchoolDatasetId,
                        principalTable: "SchoolDatasets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PupilAttendances_SchoolDatasetId",
                table: "PupilAttendances",
                column: "SchoolDatasetId");

            migrationBuilder.CreateIndex(
                name: "IX_PupilExclusions_SchoolDatasetId",
                table: "PupilExclusions",
                column: "SchoolDatasetId");

            migrationBuilder.CreateIndex(
                name: "IX_Pupils_SchoolDatasetId",
                table: "Pupils",
                column: "SchoolDatasetId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PupilAttendances");

            migrationBuilder.DropTable(
                name: "PupilExclusions");

            migrationBuilder.DropTable(
                name: "Pupils");

            migrationBuilder.DropTable(
                name: "SchoolDatasets");
        }
    }
}
