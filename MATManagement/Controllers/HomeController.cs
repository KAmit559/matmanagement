﻿using MATManagement.DatabaseService;
using MATManagement.DataModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace MATManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly Repository repo;
        private readonly ILogger<HomeController> logger;

        #region Constructor
        public HomeController(Repository repo, ILogger<HomeController> logger)
        {
            this.repo = repo;
            this.logger = logger;
        }
        #endregion

        #region Get Actions

        /// <summary>
        /// Return all SchoolDatasets
        /// </summary>
        /// <returns></returns>
        [HttpGet("datasets")]
        public async Task<IActionResult> GetAll() => Ok(await repo.GetAllSchoolDatasets());


        /// <summary>
        /// Return all active SchoolDatasets
        /// </summary>
        /// <returns></returns>
        [HttpGet("datasets/active")]
        public async Task<IActionResult> GetAllActive() => Ok(await repo.GetAllActiveSchoolDatasets());

        #endregion

        #region Post Actions

        /// <summary>
        /// Save schoolDataset and other relates set/s informatioin.
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///  Note: datasetTableName allowed only PupilExclusions, PupilAttendance and Pupil values.
        /// 
        ///     POST /datasets
        ///      {
        ///         "dataset": {
        ///             "schoolShortName": "Woodfields",
        ///             "schoolFullName": "Woodfields Infant and Junior School",
        ///             "schoolUrn": 303406,
        ///             "datasetTableName": "PupilExclusions", // Should have only PupilExclusions, PupilAttendance and Pupil.
        ///             "datasetName": "Exclusionseer",
        ///             "extractDate": "2018-08-26T11:45:00",
        ///             "periodStart": "2010-01-01T00:00:00",
        ///             "periodEnd": "2018-08-26T00:00:00",
        ///             "description": "",
        ///             "updatedAt": "2018-09-17T09:42:53.3315783+05:30",
        ///             "updatedBy": null
        ///         },
        ///         "data": [
        ///             {
        ///                Fields from PupilExclusions or PupilAttendance or Pupil dataset based on datasetTableName, take reference of wrapper in Models section.
        ///             } 
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param name="data">a wrapper of schooldataset and other datasets based on datasetTableName </param>
        /// <returns></returns>
        /// <response code="4OO">If any validation failed.</response>  
        /// <response code="422">If anything goes wrong.</response>  
        [HttpPost("datasets")]
        [ProducesResponseType(201, Type = typeof(WrapperModel))]
        public async Task<IActionResult> Add([FromBody] WrapperModel data)
        {
            logger.LogInformation("New SchoolDataset Requested..");

            var schoolDataset = data.SchoolDataset;
            schoolDataset.Ative = true;
            schoolDataset.UpdatedAt = DateTime.Now;

            try
            {
                switch (data.SchoolDataset.DatasetTableName?.ToUpper())
                {
                    case "PUPILEXCLUSIONS":

                        var pupilExclusionData = ParseDataObjects<PupilExclusion>(data.Data);

                        if (ValidateData(pupilExclusionData))
                        {
                            schoolDataset.PupilExclusions.AddRange(pupilExclusionData);
                        }

                        break;

                    case "PUPILATTENDANCE":
                        var pupilAttendanceData = ParseDataObjects<PupilAttendance>(data.Data);

                        if (ValidateData(pupilAttendanceData))
                        {
                            schoolDataset.PupilAttendances.AddRange(pupilAttendanceData);
                        }

                        break;

                    case "PUPIL":

                        var pupilsData = ParseDataObjects<Pupil>(data.Data);

                        if (ValidateData(pupilsData))
                        {
                            schoolDataset.Pupils.AddRange(pupilsData);
                        }

                        break;

                    default:
                        ModelState.AddModelError("DatasetTableName", "Invalid datasetTableName has been provided.");
                        break;
                }

                if (!ModelState.IsValid) return BadRequest(ModelState);

                await repo.AddDataset(schoolDataset);

            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message, data);

                return StatusCode(422, new ErrorModel { Message = "There was a problem with the request.", Errors = new List<string> { ex.Message } });
            }

            return StatusCode(201, data);
        }
        #endregion

        #region Private Methods

        private List<TModel> ParseDataObjects<TModel>(List<object> objects)
        {
            List<TModel> dataList = new List<TModel>();
            foreach (var item in objects)
            {
                if (item is TModel)
                {
                    dataList.Add(((TModel)item));
                }
                else
                {
                    dataList.Add(JsonConvert.DeserializeObject<TModel>(item.ToString()));
                }
            }

            return dataList;
        }

        /// <summary>
        /// Validate Deserialize List
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        private bool ValidateData(object obj)
        {
            bool valid = true;

            if (!(obj is IList objects) || objects.Count <= 0) return valid;

            var results = new List<ValidationResult>();

            foreach (var item in objects)
            {
                var context = new ValidationContext(item, null, null);

                results.Clear();

                var IsValid = Validator.TryValidateObject(item, context, results, validateAllProperties: true);

                if (!IsValid)
                {
                    valid = false;

                    foreach (var result in results)
                    {
                        foreach (var name in result.MemberNames)
                        {
                            ModelState.AddModelError($"data[{objects.IndexOf(item)}].{name}", result.ErrorMessage);
                        }
                    }
                }
            }

            return valid;
        }

        #endregion
    }

}

