﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MATManagement
{
    public class StringRangeAttribute : ValidationAttribute
    {
        public string[] AllowableValues { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (AllowableValues?.Contains(value?.ToString()) == true)
            {
                return ValidationResult.Success;
            }

            string msg = string.Empty;

            if (!string.IsNullOrWhiteSpace(ErrorMessage))
            {
                msg = ErrorMessage;
            }
            else
            {
                msg = $"Please enter one of the allowable values: {string.Join(", ", (AllowableValues ?? new string[] { "No allowable values found" }))}";
            }

            return new ValidationResult(ErrorMessage, new string[] { validationContext.MemberName });
        }
    }
}
